EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F2:STM32F205VCTx U1
U 1 1 601FF7D1
P 4950 3825
F 0 "U1" H 5600 6375 50  0000 C CNN
F 1 "STM32F205VCTx" H 4950 3825 50  0000 C CNN
F 2 "Package_QFP:LQFP-100_14x14mm_P0.5mm" H 4250 1225 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00237391.pdf" H 4950 3825 50  0001 C CNN
	1    4950 3825
	1    0    0    -1  
$EndComp
Text GLabel 5900 2625 2    50   BiDi ~ 0
USB_FS_D+
Text GLabel 5900 2525 2    50   BiDi ~ 0
USB_FS_D-
Text GLabel 5900 2425 2    50   BiDi ~ 0
USB_FS_ID
Wire Wire Line
	5900 2625 5850 2625
Wire Wire Line
	5900 2525 5850 2525
Wire Wire Line
	5900 2425 5850 2425
Text GLabel 5900 4625 2    50   BiDi ~ 0
USB_HS_D+
Wire Wire Line
	5900 4625 5850 4625
Text GLabel 5900 4525 2    50   BiDi ~ 0
USB_HS_D-
Wire Wire Line
	5900 4525 5850 4525
Text GLabel 5900 4325 2    50   BiDi ~ 0
USB_HS_ID
Wire Wire Line
	5900 4325 5850 4325
Text GLabel 4000 2825 0    50   BiDi ~ 0
RCC_OSC_IN
Wire Wire Line
	4000 2825 4050 2825
Text GLabel 4000 2925 0    50   BiDi ~ 0
RCC_OSC_OUT
Wire Wire Line
	4000 2925 4050 2925
Text GLabel 5900 3825 2    50   BiDi ~ 0
USART1_RX
Wire Wire Line
	5900 3825 5850 3825
Text GLabel 5900 2325 2    50   BiDi ~ 0
USART1_TX
Wire Wire Line
	5850 2325 5900 2325
Text GLabel 5900 5725 2    50   BiDi ~ 0
I2C3_SDA
Wire Wire Line
	5900 5725 5850 5725
Text GLabel 5900 2225 2    50   BiDi ~ 0
I2C3_SCL
Wire Wire Line
	5900 2225 5850 2225
Text GLabel 5900 2725 2    50   BiDi ~ 0
SYS_JTMS-SWDIO
Wire Wire Line
	5900 2725 5850 2725
Text GLabel 5900 2825 2    50   BiDi ~ 0
SYS_JTCK-SWCLK
Wire Wire Line
	5850 2825 5900 2825
Text GLabel 5900 2925 2    50   BiDi ~ 0
SYS_JTDI
Wire Wire Line
	5900 2925 5850 2925
Text GLabel 5900 5825 2    50   BiDi ~ 0
SPI3_SCK
Wire Wire Line
	5900 5825 5850 5825
Text GLabel 5900 5925 2    50   BiDi ~ 0
SPI3_MISO
Wire Wire Line
	5850 5925 5900 5925
Text GLabel 5900 6025 2    50   BiDi ~ 0
SPI3_MOSI
Wire Wire Line
	5850 6025 5900 6025
Text GLabel 4000 4825 0    50   BiDi ~ 0
CAN1_RX
Wire Wire Line
	4000 4825 4050 4825
Text GLabel 4000 4925 0    50   BiDi ~ 0
CAN1_TX
Wire Wire Line
	4000 4925 4050 4925
Text GLabel 5900 3425 2    50   BiDi ~ 0
SYS_JTDO_SWO
Wire Wire Line
	5850 3425 5900 3425
Text GLabel 5900 3525 2    50   BiDi ~ 0
SYS_JTRST
Wire Wire Line
	5850 3525 5900 3525
Text GLabel 5900 3625 2    50   BiDi ~ 0
CAN2_RX
Wire Wire Line
	5850 3625 5900 3625
Text GLabel 5900 3725 2    50   BiDi ~ 0
I2C1_SCL
Wire Wire Line
	5900 3725 5850 3725
Text GLabel 5900 4025 2    50   BiDi ~ 0
I2C1_SDA
Wire Wire Line
	5900 4025 5850 4025
Text GLabel 4000 5125 0    50   BiDi ~ 0
USART3_TX
Wire Wire Line
	4000 5125 4050 5125
Text GLabel 5900 4425 2    50   BiDi ~ 0
CAN2_TX
Wire Wire Line
	5900 4425 5850 4425
Text GLabel 5900 4225 2    50   BiDi ~ 0
USART3_RX
Wire Wire Line
	5850 4225 5900 4225
Text GLabel 5900 4125 2    50   BiDi ~ 0
SPI2_SCK
Wire Wire Line
	5900 4125 5850 4125
Text GLabel 5900 2125 2    50   BiDi ~ 0
SPI1_MOSI
Wire Wire Line
	5850 2125 5900 2125
Text GLabel 5900 2025 2    50   BiDi ~ 0
SPI1_MISO
Wire Wire Line
	5900 2025 5850 2025
Text GLabel 5900 1925 2    50   BiDi ~ 0
SPI1_SCK
Wire Wire Line
	5850 1925 5900 1925
Text GLabel 5900 1825 2    50   BiDi ~ 0
SPI1_NSS
Wire Wire Line
	5850 1825 5900 1825
Text GLabel 5900 1725 2    50   BiDi ~ 0
USART2_RX
Wire Wire Line
	5850 1725 5900 1725
Text GLabel 5900 1625 2    50   BiDi ~ 0
USART2_TX
Wire Wire Line
	5900 1625 5850 1625
Text GLabel 5900 1525 2    50   BiDi ~ 0
ADC1_IN1
Text GLabel 5900 1425 2    50   BiDi ~ 0
ADC1_IN0
Wire Wire Line
	5850 1425 5900 1425
Wire Wire Line
	5850 1525 5900 1525
Text GLabel 5900 5025 2    50   BiDi ~ 0
SPI2_MISO
Text GLabel 5900 5125 2    50   BiDi ~ 0
SPI2_MOSI
Wire Wire Line
	5900 5025 5850 5025
Wire Wire Line
	5850 5125 5900 5125
Text GLabel 4000 1625 0    50   BiDi ~ 0
BOOT0
Text GLabel 4000 1425 0    50   BiDi ~ 0
NRST
Wire Wire Line
	4000 1425 4050 1425
Wire Wire Line
	4000 1625 4050 1625
$Comp
L power:GND #PWR0101
U 1 1 60279A34
P 4850 6700
F 0 "#PWR0101" H 4850 6450 50  0001 C CNN
F 1 "GND" H 4855 6527 50  0000 C CNN
F 2 "" H 4850 6700 50  0001 C CNN
F 3 "" H 4850 6700 50  0001 C CNN
	1    4850 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6700 4850 6675
Wire Wire Line
	5050 6625 5050 6675
Wire Wire Line
	5050 6675 4950 6675
Connection ~ 4850 6675
Wire Wire Line
	4850 6675 4850 6625
Wire Wire Line
	4950 6625 4950 6675
Connection ~ 4950 6675
Wire Wire Line
	4950 6675 4850 6675
$Comp
L power:VDD #PWR0102
U 1 1 6027DA9C
P 4750 1000
F 0 "#PWR0102" H 4750 850 50  0001 C CNN
F 1 "VDD" H 4765 1173 50  0000 C CNN
F 2 "" H 4750 1000 50  0001 C CNN
F 3 "" H 4750 1000 50  0001 C CNN
	1    4750 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1000 4750 1075
Wire Wire Line
	4750 1075 4850 1075
Wire Wire Line
	4850 1075 4850 1125
Connection ~ 4750 1075
Wire Wire Line
	4750 1075 4750 1125
Wire Wire Line
	4850 1075 4950 1075
Wire Wire Line
	5250 1075 5250 1125
Connection ~ 4850 1075
Wire Wire Line
	5150 1125 5150 1075
Connection ~ 5150 1075
Wire Wire Line
	5150 1075 5250 1075
Wire Wire Line
	5050 1125 5050 1075
Connection ~ 5050 1075
Wire Wire Line
	5050 1075 5150 1075
Wire Wire Line
	4950 1125 4950 1075
Connection ~ 4950 1075
Wire Wire Line
	4950 1075 5050 1075
$Comp
L Device:Crystal Y1
U 1 1 60286697
P 2175 2425
F 0 "Y1" H 2175 2693 50  0000 C CNN
F 1 "25 MHz" H 2175 2602 50  0000 C CNN
F 2 "" H 2175 2425 50  0001 C CNN
F 3 "~" H 2175 2425 50  0001 C CNN
	1    2175 2425
	1    0    0    -1  
$EndComp
Text GLabel 2200 2775 0    50   BiDi ~ 0
RCC_OSC_IN
Text GLabel 2300 2950 0    50   BiDi ~ 0
RCC_OSC_OUT
$EndSCHEMATC
